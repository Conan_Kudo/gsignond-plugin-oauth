OAuth 1.0/2.0 Plugin for the gSignOn daemon
===========================================

This plugin for the Accounts-SSO gSignOn daemon handles the OAuth 1.0 and 2.0 authentication protocols.


Build instructions
------------------

This project depends on GSignond, and uses the Meson build system. To build it, run
```
meson build --prefix=/usr
cd build
ninja
sudo ninja install
```

License
-------

See COPYING.LIB file.

Resources
---------

[OAuth 1.0/2.0 Plugin documentation](http://accounts-sso.gitlab.io/gsignond-plugin-oa/index.html)

[Official source code repository](https://gitlab.com/accounts-sso/gsignond-plugin-oa)
