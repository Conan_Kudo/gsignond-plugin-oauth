/* vi: set et sw=4 ts=4 cino=t0,(0: */
/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of gsignond
 *
 * Copyright (C) 2012 Intel Corporation.
 *
 * Contact: Alexander Kanavin <alex.kanavin@gmail.com>
 *          Amarnath Valluri <amarnath.valluri@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "oauthtest_utils.h"

SoupServer* _new_server()
{
    // to genenerate cert and key
    // openssl genrsa -out privkey.pem 2048
    // openssl req -new -x509 -key privkey.pem -out cacert.pem -days 365000
#ifdef SOUP_VERSION_2_48
    GTlsCertificate *tls = g_tls_certificate_new_from_files("cacert.pem",
                                                            "privkey.pem", NULL);
    SoupServer* server = soup_server_new(SOUP_SERVER_TLS_CERTIFICATE, tls, NULL);
    g_object_unref(tls);
#else
    SoupServer* server = soup_server_new(SOUP_SERVER_SSL_CERT_FILE, "cacert.pem",
                                         SOUP_SERVER_SSL_KEY_FILE, "privkey.pem",
                                         NULL);
#endif

    return server;
}

gboolean _start_server(SoupServer* server)
{
#ifdef SOUP_VERSION_2_48
    return soup_server_listen_local(server, 0, SOUP_SERVER_LISTEN_HTTPS,
                                     NULL);
#else
    return soup_server_run_async(server);
#endif
}

guint _get_server_port(SoupServer* server)
{
    guint port = 0;
#ifdef SOUP_VERSION_2_48
    GSList *uris = soup_server_get_uris(server);
    if (!uris) return 0;
    port = soup_uri_get_port((SoupURI*)uris->data);
    g_slist_free_full(uris, (GDestroyNotify)soup_uri_free);
#else
    port = soup_server_get_port(server);
#endif
    return port;
}

